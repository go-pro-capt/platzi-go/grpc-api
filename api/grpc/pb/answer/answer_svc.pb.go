// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.12.4
// source: answer/answer_svc.proto

package answer

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

var File_answer_answer_svc_proto protoreflect.FileDescriptor

var file_answer_answer_svc_proto_rawDesc = []byte{
	0x0a, 0x17, 0x61, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x2f, 0x61, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x5f,
	0x73, 0x76, 0x63, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x06, 0x61, 0x6e, 0x73, 0x77, 0x65,
	0x72, 0x1a, 0x13, 0x61, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x2f, 0x61, 0x6e, 0x73, 0x77, 0x65, 0x72,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x32, 0xc9, 0x02, 0x0a, 0x0d, 0x41, 0x6e, 0x73, 0x77, 0x65,
	0x72, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x3a, 0x0a, 0x09, 0x53, 0x65, 0x74, 0x41,
	0x6e, 0x73, 0x77, 0x65, 0x72, 0x12, 0x0e, 0x2e, 0x61, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x2e, 0x41,
	0x6e, 0x73, 0x77, 0x65, 0x72, 0x1a, 0x19, 0x2e, 0x61, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x2e, 0x53,
	0x65, 0x74, 0x41, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x28, 0x01, 0x30, 0x01, 0x12, 0x5a, 0x0a, 0x13, 0x47, 0x65, 0x74, 0x41, 0x6e, 0x73, 0x77, 0x65,
	0x72, 0x73, 0x42, 0x79, 0x53, 0x74, 0x75, 0x64, 0x65, 0x6e, 0x74, 0x12, 0x18, 0x2e, 0x61, 0x6e,
	0x73, 0x77, 0x65, 0x72, 0x2e, 0x47, 0x65, 0x74, 0x41, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x25, 0x2e, 0x61, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x2e, 0x53,
	0x68, 0x6f, 0x77, 0x41, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x73, 0x50, 0x65, 0x72, 0x53, 0x74, 0x75,
	0x64, 0x65, 0x6e, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x28, 0x01, 0x30, 0x01,
	0x12, 0x5b, 0x0a, 0x14, 0x47, 0x65, 0x74, 0x41, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x73, 0x42, 0x79,
	0x51, 0x75, 0x65, 0x73, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x18, 0x2e, 0x61, 0x6e, 0x73, 0x77, 0x65,
	0x72, 0x2e, 0x47, 0x65, 0x74, 0x41, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x1a, 0x25, 0x2e, 0x61, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x2e, 0x53, 0x68, 0x6f, 0x77,
	0x41, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x50, 0x65, 0x72, 0x51, 0x75, 0x65, 0x73, 0x74, 0x69, 0x6f,
	0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x28, 0x01, 0x30, 0x01, 0x12, 0x43, 0x0a,
	0x08, 0x53, 0x65, 0x74, 0x53, 0x63, 0x6f, 0x72, 0x65, 0x12, 0x18, 0x2e, 0x61, 0x6e, 0x73, 0x77,
	0x65, 0x72, 0x2e, 0x47, 0x65, 0x74, 0x41, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x61, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x2e, 0x53, 0x65, 0x74,
	0x41, 0x6e, 0x73, 0x77, 0x65, 0x72, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x28, 0x01,
	0x30, 0x01, 0x42, 0x3e, 0x5a, 0x3c, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d,
	0x2f, 0x67, 0x6f, 0x2d, 0x70, 0x72, 0x6f, 0x2d, 0x63, 0x61, 0x70, 0x74, 0x2f, 0x70, 0x6c, 0x61,
	0x74, 0x7a, 0x69, 0x2d, 0x67, 0x6f, 0x2f, 0x67, 0x72, 0x70, 0x63, 0x2d, 0x61, 0x70, 0x69, 0x2f,
	0x61, 0x70, 0x69, 0x2f, 0x67, 0x72, 0x70, 0x63, 0x2f, 0x70, 0x62, 0x2f, 0x61, 0x6e, 0x73, 0x77,
	0x65, 0x72, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var file_answer_answer_svc_proto_goTypes = []interface{}{
	(*Answer)(nil),                        // 0: answer.Answer
	(*GetAnswerRequest)(nil),              // 1: answer.GetAnswerRequest
	(*SetAnswerResponse)(nil),             // 2: answer.SetAnswerResponse
	(*ShowAnswersPerStudentResponse)(nil), // 3: answer.ShowAnswersPerStudentResponse
	(*ShowAnswerPerQuestionResponse)(nil), // 4: answer.ShowAnswerPerQuestionResponse
}
var file_answer_answer_svc_proto_depIdxs = []int32{
	0, // 0: answer.AnswerService.SetAnswer:input_type -> answer.Answer
	1, // 1: answer.AnswerService.GetAnswersByStudent:input_type -> answer.GetAnswerRequest
	1, // 2: answer.AnswerService.GetAnswersByQuestion:input_type -> answer.GetAnswerRequest
	1, // 3: answer.AnswerService.SetScore:input_type -> answer.GetAnswerRequest
	2, // 4: answer.AnswerService.SetAnswer:output_type -> answer.SetAnswerResponse
	3, // 5: answer.AnswerService.GetAnswersByStudent:output_type -> answer.ShowAnswersPerStudentResponse
	4, // 6: answer.AnswerService.GetAnswersByQuestion:output_type -> answer.ShowAnswerPerQuestionResponse
	2, // 7: answer.AnswerService.SetScore:output_type -> answer.SetAnswerResponse
	4, // [4:8] is the sub-list for method output_type
	0, // [0:4] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_answer_answer_svc_proto_init() }
func file_answer_answer_svc_proto_init() {
	if File_answer_answer_svc_proto != nil {
		return
	}
	file_answer_answer_proto_init()
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_answer_answer_svc_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   0,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_answer_answer_svc_proto_goTypes,
		DependencyIndexes: file_answer_answer_svc_proto_depIdxs,
	}.Build()
	File_answer_answer_svc_proto = out.File
	file_answer_answer_svc_proto_rawDesc = nil
	file_answer_answer_svc_proto_goTypes = nil
	file_answer_answer_svc_proto_depIdxs = nil
}
