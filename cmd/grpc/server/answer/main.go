package main

import (
	"log"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/grpc/server"
)

func main() {
	log.Println("Answer Server is running...")
	server.RunGrpcServer("answer")
}
