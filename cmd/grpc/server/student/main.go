package main

import (
	"fmt"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/grpc/server"
)

func main() {
	fmt.Println("StudentServer is running ...")
	server.RunGrpcServer("student")
}
