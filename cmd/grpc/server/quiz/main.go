package main

import (
	"fmt"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/grpc/server"
)

func main() {
	fmt.Println("QuizServer is running")
	server.RunGrpcServer("quiz")
}
