package main

import (
	"fmt"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/grpc/server"
)

func main() {
	fmt.Println("ScoreServer is running ...")
	server.RunGrpcServer("score")
}
