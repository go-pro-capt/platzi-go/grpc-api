package ports

import (
	"context"
	"io"
	"log"
	"time"

	"github.com/segmentio/ksuid"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/api/grpc/pb/enrollment"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
	gw_answer "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/answer/gateway"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/enrollment/gateway"
	gw_quiz "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/quiz/gateway"
)

type EnrollmentServer struct {
	enrollment.UnimplementedEnrollmentServiceServer
	Gw gateway.AppGateway
}

func (s *EnrollmentServer) EnrollQuiz(ctx context.Context, req *enrollment.QuizEnrollments) (*enrollment.SetQuizEnrollmentResponse, error) {
	enrollmentGW := models.Enrollment{
		StudentId:      req.GetStudentId(),
		QuizId:         req.GetQuizId(),
		EnrollmentDate: time.Now(),
		EnrollmentCode: req.GetEnrollmentCode(),
	}

	err := s.Gw.SetEnrollments(ctx, &enrollmentGW)

	if err != nil {
		log.Println(err.Error())
	}

	return &enrollment.SetQuizEnrollmentResponse{StudentEnrolled: enrollmentGW.StudentId}, nil
}

func (s *EnrollmentServer) TakeQuiz(stream enrollment.EnrollmentService_TakeQuizServer) error {
	// var currentEnrolledStudent = &models.Enrollment{}
	for {
		quiz, err := stream.Recv()

		if err == io.EOF {
			return nil
		}

		if err != nil {
			log.Println(err.Error())
		}
		enrolledStudents, err := gw_quiz.QuizGW.GetEnrollmentsByQuiz(context.Background(), quiz.GetQuizId())
		// log.Println("ER", enrolledStudents)
		if err != nil {
			log.Println(err.Error())
			return err
		}

		questions, err := gw_quiz.QuizGW.GetQuestionsByQuiz(context.Background(), quiz.GetQuizId())

		if questions == nil && err != nil {
			log.Println(err.Error())
			return err
		}
		ies := 0
		for {
			if enrolledStudents != nil && enrolledStudents[ies].StudentId == quiz.GetStudentId() {
				if ValidateQuizIntents(quiz.GetQuizId(), int(quiz.GetEnrollmentId())) {
					log.Println("EnrolledStudent: ", enrolledStudents[ies].Id)

					err = StartQuiz(enrolledStudents[ies], questions, stream)
					if err != nil {
						log.Println(err.Error())
						break
					} else {
						return nil // ensure current student doesn't take the quiz more than once
					}
				} else { // end If ValidateQuizIntents
					log.Println("You only have one intent to take the quiz")
					return nil
				}
			} else if ies < len(enrolledStudents)-1 {
				ies++
			} else {
				log.Println("You are not enrolled in this quiz")
				return nil
			}
		}
	}
}

func StartQuiz(enrolledStudent *models.Enrollment, questions []*models.Question, stream enrollment.EnrollmentService_TakeQuizServer) error {
	lenQuestions := len(questions) - 1
	var currentQuestion = &models.Question{}
	i := 0
	for {
		if i <= lenQuestions {
			currentQuestion = questions[i]
			questionToSend := enrollment.SetTakeQuizResponse{
				QuestionStament: currentQuestion.Statement,
			}
			err := stream.Send(&questionToSend)

			if err != nil {
				return err
			}

			answer, err := stream.Recv()

			if err == io.EOF {
				return nil

			}

			if err != nil {
				log.Println(err.Error())
			}
			id, err := ksuid.NewRandom()
			if err != nil {
				log.Println(err.Error())
			}
			key := "ans-"
			ans := models.Answer{
				Id:           key + id.String(),
				Body:         answer.AnswerBody,
				QuestionId:   currentQuestion.Id,
				EnrollmentId: enrolledStudent.Id,
			}

			log.Println("ANS: ", ans)
			err = gw_answer.AnswerGW.SetAnswer(context.Background(), &ans)
			if err != nil {
				log.Println(err.Error())
			}

			i++
		} else {
			log.Printf("Dear Student %s, your quiz has finished", enrolledStudent.StudentId)
			return nil
		}
	}
}

func ValidateQuizIntents(quizId string, enrollmentId int) bool {
	QzAnswers, err := gw_answer.AnswerGW.GetAnswersByStudent(context.Background(), quizId, enrollmentId)
	// log.Println("ANS", QzAnswers)
	if err != nil {
		log.Println(err.Error())
		return false
	}
	if QzAnswers != nil {
		return false
	} else {
		return true
	}
}
