package gateway

import (
	"context"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
)

type AppGateway struct {
	db MainRepo
}

var EnrollmentGW = AppGateway{db: &MariadbGW{}}

func (gw *AppGateway) SetEnrollments(ctx context.Context, enrollment *models.Enrollment) error {
	return gw.db.set(ctx, enrollment)
}
