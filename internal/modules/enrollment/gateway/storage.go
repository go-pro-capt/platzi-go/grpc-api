package gateway

import (
	"context"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/common/database/mariadb"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
)

type MainRepo interface {
	set(ctx context.Context, enrollment *models.Enrollment) error
}

var mdbC, _ = mariadb.GetConnection()

type MariadbGW struct{}

func (mdbgw *MariadbGW) set(ctx context.Context, enrollment *models.Enrollment) error {
	query := "INSERT INTO enrollments(student_id, quiz_id, enrollment_date, enrollment_code) VALUES(?, ?, ?, ?)"
	_, err := mdbC.ExecContext(ctx, query, enrollment.StudentId, enrollment.QuizId, enrollment.EnrollmentDate, enrollment.EnrollmentCode)

	return err
}
