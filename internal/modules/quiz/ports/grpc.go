package ports

import (
	"context"
	"log"
	"time"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/api/grpc/pb/enrollment"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/api/grpc/pb/question"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/api/grpc/pb/quiz"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/quiz/gateway"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type QuizServer struct {
	quiz.UnimplementedQuizServiceServer
	Gw gateway.AppGateway
}

func (s *QuizServer) GetQuiz(ctx context.Context, req *quiz.GetQuizRequest) (*quiz.Quiz, error) {
	quizModel, err := s.Gw.GetQuiz(ctx, "id", req.GetId())

	if quizModel == nil {
		log.Printf("%s Quiz not found", req.GetId())
		return &quiz.Quiz{}, err
	}

	quizm := quizModel.(*models.Quiz)
	quizm.CreatedAt = time.Now()

	return &quiz.Quiz{
		Id:        quizm.Id,
		Name:      quizm.Name,
		CreatedAt: timestamppb.New(quizm.CreatedAt),
	}, nil

}

func (s *QuizServer) SetQuiz(ctx context.Context, req *quiz.Quiz) (*quiz.SetQuizResponse, error) {
	qzm := &models.Quiz{
		Id:        req.GetId(),
		Name:      req.GetName(),
		CreatedAt: time.Now(),
	}
	err := s.Gw.SetQuiz(ctx, qzm)

	if err != nil {
		log.Println(err.Error())
		return &quiz.SetQuizResponse{}, err
	}
	return &quiz.SetQuizResponse{
		Id:   qzm.Id,
		Name: qzm.Name,
	}, nil
}

func (s *QuizServer) GetQuestionsByQuiz(ctx context.Context, req *quiz.GetQuizRequest) (*question.SetQuestionPerQuizResponse, error) {
	questionsGW, err := s.Gw.GetQuestionsByQuiz(ctx, req.GetId())
	questionsRPC := make([]*question.Question, len(questionsGW))

	if questionsGW == nil && err != nil {
		log.Println(err.Error())
		return &question.SetQuestionPerQuizResponse{}, nil
	}
	for i, qs := range questionsGW {
		questionsRPC[i] = &question.Question{
			Id:        qs.Id,
			Name:      qs.Name,
			QuizId:    qs.QuizId,
			Statement: qs.Statement,
		}
	}
	return &question.SetQuestionPerQuizResponse{
		Questions: questionsRPC,
	}, nil
}

func (s *QuizServer) GetEnrolledStudentsByQuiz(ctx context.Context, req *quiz.GetQuizRequest) (*enrollment.SetEnrolledStudentsResponse, error) {
	enrollmentsGW, err := s.Gw.GetEnrollmentsByQuiz(ctx, req.GetId())
	enrollmentsPB := make([]*enrollment.QuizEnrollments, len(enrollmentsGW))

	if enrollmentsGW == nil && err != nil {
		log.Println(err.Error())
		return &enrollment.SetEnrolledStudentsResponse{}, nil
	}
	for i, estd := range enrollmentsGW {
		enrollmentsPB[i] = &enrollment.QuizEnrollments{
			EnrollmentCode: estd.EnrollmentCode,
			StudentId:      estd.StudentId,
			EnrollmentDate: timestamppb.New(estd.EnrollmentDate),
		}
	}
	return &enrollment.SetEnrolledStudentsResponse{
		EnrolledStudents: enrollmentsPB,
	}, nil
}

// func ShowQuizResults(quiz.GetQuizRequest) returns (quiz.SetQuizResultsResponse) {
//
// 	return nil
// }
