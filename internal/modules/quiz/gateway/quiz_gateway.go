package gateway

import (
	"context"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
)

type AppGateway struct {
	db MainRepo
}

var QuizGW = AppGateway{db: &MariadbGW{}}

func (gw *AppGateway) GetQuiz(ctx context.Context, flag string, value string) (models.Model, error) {
	return gw.db.getBy(ctx, flag, value)
}

func (gw *AppGateway) SetQuiz(ctx context.Context, qm *models.Quiz) error {
	return gw.db.set(ctx, qm)
}

func (gw *AppGateway) GetQuestionsByQuiz(ctx context.Context, quiz_id string) ([]*models.Question, error) {
	return gw.db.getQuestionsByQuiz(ctx, quiz_id)
}

func (gw *AppGateway) GetEnrollmentsByQuiz(ctx context.Context, quiz_id string) ([]*models.Enrollment, error) {
	return gw.db.getEnrollmentsByQuiz(ctx, quiz_id)
}
