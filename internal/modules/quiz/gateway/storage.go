package gateway

import (
	"context"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/common/database/mariadb"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/utils/db"
)

type MainRepo interface {
	set(ctx context.Context, qm *models.Quiz) error
	getBy(ctx context.Context, flag string, value string) (models.Model, error)
	getQuestionsByQuiz(ctx context.Context, id string) ([]*models.Question, error)
	getEnrollmentsByQuiz(ctx context.Context, id string) ([]*models.Enrollment, error)
}

var mdbC, _ = mariadb.GetConnection()

type MariadbGW struct{}

func (gw *MariadbGW) getBy(ctx context.Context, flag string, value string) (models.Model, error) {
	qzModel, _ := db.FindQuizById(ctx, value, mdbC) // Quiz has been found By their ID
	return qzModel, nil
}

func (mdbgw *MariadbGW) set(ctx context.Context, quiz *models.Quiz) error {
	query := "INSERT INTO quizes(id, name, created_at) VALUES(?, ?, ?)"
	_, err := mdbC.ExecContext(ctx, query, quiz.Id, quiz.Name, quiz.CreatedAt)

	return err
}

func (mdbgw *MariadbGW) getQuestionsByQuiz(ctx context.Context, id string) ([]*models.Question, error) {
	questions, _ := db.GetQuestionsByQuizId(ctx, id, mdbC) // Questions belong to a particular Quiz
	return questions, nil
}

func (mdbgw *MariadbGW) getEnrollmentsByQuiz(ctx context.Context, id string) ([]*models.Enrollment, error) {
	enrollments, _ := db.GetEnrolledStudents(ctx, id, mdbC) // Questions belong to a particular Quiz
	return enrollments, nil
}
