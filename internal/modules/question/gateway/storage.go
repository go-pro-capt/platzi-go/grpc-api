package gateway

import (
	"context"
	"log"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/common/database/mariadb"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/utils/db"
)

type MainRepo interface {
	getBy(ctx context.Context, flag string, value db.Values) (models.Model, []*models.Question, error)
	set(ctx context.Context, question *models.Question) error
}

var mdbC, _ = mariadb.GetConnection()

type MariadbGW struct{}

func (gw *MariadbGW) getBy(ctx context.Context, flag string, value db.Values) (models.Model, []*models.Question, error) {
	switch flag {
	case "id":
		stdModel, _ := db.FindQuestionById(ctx, value, mdbC) // Question has been found By their ID
		return stdModel, nil, nil
	}
	return nil, nil, nil
}

func (mdbgw *MariadbGW) set(ctx context.Context, qs *models.Question) error {
	// Question Model
	query := "INSERT INTO questions(id, name, statement, quiz_id) VALUES(?, ?, ?, ?)"
	_, err := mdbC.ExecContext(ctx, query, qs.Id, qs.Name, qs.Statement, qs.QuizId)

	if err != nil {
		log.Println(err.Error())
	}
	return nil

}
