package gateway

import (
	"context"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/utils/db"
)

type AppGateway struct {
	db MainRepo
}

var QuestionGW = AppGateway{db: &MariadbGW{}}

func (gw *AppGateway) GetQuestion(ctx context.Context, flag string, value db.Values) (models.Model, []*models.Question, error) {
	return gw.db.getBy(ctx, flag, value)
}

func (gw *AppGateway) SetQuestion(ctx context.Context, qm *models.Question) error {
	return gw.db.set(ctx, qm)
}
