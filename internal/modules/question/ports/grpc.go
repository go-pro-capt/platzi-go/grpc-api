package ports

import (
	"context"
	"io"
	"log"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/api/grpc/pb/question"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/question/gateway"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/utils/db"
)

type QuestionServer struct {
	question.UnimplementedQuestionServiceServer
	Gw gateway.AppGateway
}

func (s *QuestionServer) SetQuestion(stream question.QuestionService_SetQuestionServer) error {
	for {
		q, err := stream.Recv()

		if err == io.EOF {
			return stream.SendAndClose(&question.SetQuestionResponse{Ok: true})
		}

		if err != nil {
			log.Println(err.Error())
			return err
		}

		qsModel := models.Question{
			Id:        q.GetId(),
			Name:      q.GetName(),
			Statement: q.GetStatement(),
			QuizId:    q.GetQuizId(),
		}

		log.Println("Question saved: ", qsModel.Id)
		err = s.Gw.SetQuestion(context.Background(), &qsModel)

		if err != nil {
			log.Println(err.Error())
			return stream.SendAndClose(&question.SetQuestionResponse{Ok: false})
		}

	}
}

func (s *QuestionServer) GetQuestion(ctx context.Context, req *question.GetQuestionRequest) (*question.Question, error) {
	values := db.Values{Id: req.GetId(), QuizId: req.GetQuizId()}
	qs, _, err := s.Gw.GetQuestion(ctx, "id", values)

	if qs == nil && err != nil {
		log.Println(err.Error())
		return &question.Question{}, nil
	}

	qsModel := qs.(*models.Question)

	return &question.Question{
		Id:        qsModel.Id,
		Name:      qsModel.Name,
		Statement: qsModel.Statement,
	}, nil
}
