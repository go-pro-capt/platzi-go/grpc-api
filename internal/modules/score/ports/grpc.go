package ports

import (
	"context"
	"log"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/api/grpc/pb/score"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
	quiz_gw "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/quiz/gateway"
	score_gw "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/score/gateway"
)

type ScoreServer struct {
	score.UnimplementedScoreServiceServer
	Gw score_gw.AppGateway
}

func (sv *ScoreServer) SetQuizScore(ctx context.Context, req *score.GetScoreRequest) (*score.SetScoreQuizResponse, error) {
	enrolledStudents, err := quiz_gw.QuizGW.GetEnrollmentsByQuiz(ctx, req.GetQuizId())
	if err != nil {
		log.Println(err.Error())
	}
	quizResult := 0
	for _, estd := range enrolledStudents {
		// log.Println(estd)
		if estd.StudentId == req.GetStudentId() {
			answers, err := score_gw.ScoreGW.GetScoreByQuiz(ctx, req.GetQuizId(), estd.Id)
			if answers == nil && err != nil {
				log.Println(err.Error())
				return nil, err
			}
			answerScore := 1
			correct_answers := [4]string{"a1", "a2", "a3", "a4"}
			for _, ans := range answers {
				for _, c_a := range correct_answers {
					if c_a == ans.Ans_body {
						//score = 1
						answerScore = 1
						ans_score := models.AnswerScore{AnswerId: ans.Ans_id, Score: answerScore}
						quizResult += 1
						log.Println(ans_score)
					} else {
						answerScore = 0
					}
				}
			}
			mQuizResults := models.QuizResults{
				QuizId:    req.GetQuizId(),
				StudentId: estd.StudentId,
				Score:     quizResult,
			}
			log.Println(mQuizResults)
		} // end if studentId
	} // end for enrolledStudents
	return &score.SetScoreQuizResponse{
		TotalScore: uint32(quizResult),
	}, nil
	// return nil, nil
}
