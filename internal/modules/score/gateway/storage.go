package gateway

import (
	"context"
	"log"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/common/database/mariadb"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/utils/db"
)

type MainRepo interface {
	setQuizScore(ctx context.Context, score *models.AnswerScore) error
	getScoreByQuiz(ctx context.Context, quizId string, enrollmentId int) ([]*models.QuizAnswersByStudent, error)
}

var mdbC, _ = mariadb.GetConnection()

type MariadbGW struct{}

func (gw *MariadbGW) setQuizScore(ctx context.Context, score *models.AnswerScore) error {
	query := "INSERT INTO scores(id, answer_id, score) VALUES(?, ?, ?)"
	_, err := mdbC.ExecContext(ctx, query, score.Id, score.AnswerId, score.Score)

	if err != nil {
		log.Println(err.Error())
		return err
	}

	return nil
}

func (gw *MariadbGW) getScoreByQuiz(ctx context.Context, quizId string, enrollmentId int) ([]*models.QuizAnswersByStudent, error) {
	answers, _ := db.GetQuizAnswersByQuiz(ctx, quizId, enrollmentId, mdbC)
	return answers, nil
}
