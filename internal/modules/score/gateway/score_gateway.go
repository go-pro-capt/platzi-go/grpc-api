package gateway

import (
	"context"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
)

type AppGateway struct {
	db MainRepo
}

var ScoreGW = AppGateway{db: &MariadbGW{}}

func (sw *AppGateway) SetQuizScore(ctx context.Context, score *models.AnswerScore) error {
	return sw.db.setQuizScore(ctx, score)
}

func (sw *AppGateway) GetScoreByQuiz(ctx context.Context, quizId string, enrollmentId int) ([]*models.QuizAnswersByStudent, error) {
	return sw.db.getScoreByQuiz(ctx, quizId, enrollmentId)
}
