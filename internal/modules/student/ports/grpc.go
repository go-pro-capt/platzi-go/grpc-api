package ports

import (
	"context"
	"log"
	"strconv"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/api/grpc/pb/student"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/student/gateway"
)

type StudentServer struct {
	student.UnimplementedStudentServiceServer
	Gw gateway.AppGateway
}

func (s *StudentServer) GetStudent(ctx context.Context, req *student.GetStudentRequest) (*student.Student, error) {
	std, err := s.Gw.GetStudentByFlag(ctx, "id", req.GetId())
	//
	if std == nil {
		log.Printf("%s Student not found", req.GetId())
		stda := &student.Student{}
		return stda, err
	}

	stdm := std.(*models.Student)
	Age, _ := strconv.ParseInt(stdm.Age, 10, 32)

	return &student.Student{
		Id:   stdm.Id,
		Name: stdm.Name,
		Age:  int32(Age),
	}, nil
}

func (s *StudentServer) SetStudent(ctx context.Context, req *student.Student) (*student.SetStudentResponse, error) {
	std := models.Student{
		Id:   req.GetId(),
		Name: req.GetName(),
		Age:  strconv.Itoa(int(req.GetAge())),
	}

	err := s.Gw.SetStudent(ctx, &std)

	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	return &student.SetStudentResponse{
		Id: std.Id,
	}, nil
}
