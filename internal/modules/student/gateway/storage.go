package gateway

import (
	"context"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/common/database/mariadb"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/utils/db"
)

type MainRepo interface {
	getBy(ctx context.Context, flag string, value string) (models.Model, error)
	set(ctx context.Context, student *models.Student) error
}

var mdbC, _ = mariadb.GetConnection()

type MariadbGW struct{}

// func (mDBStore *MariaDBStore) getBy(ctx context.Context, db_table string, flag string, value string) (database.Model, error) {
func (gw *MariadbGW) getBy(ctx context.Context, flag string, value string) (models.Model, error) {
	stdModel, _ := db.FindStudentById(ctx, value, mdbC) // Student has been found By their ID
	return stdModel, nil
}

func (mdbgw *MariadbGW) set(ctx context.Context, std *models.Student) error {
	query := "INSERT INTO students(id, name, age) VALUES(?, ?, ?)"
	_, err := mdbC.ExecContext(ctx, query, std.Id, std.Name, std.Age)

	return err
}
