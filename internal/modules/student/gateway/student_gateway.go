package gateway

import (
	"context"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
)

type AppGateway struct {
	db MainRepo
}

var StudentGW = AppGateway{db: &MariadbGW{}}

func (gw *AppGateway) GetStudentByFlag(ctx context.Context, flag string, value string) (models.Model, error) {
	return gw.db.getBy(ctx, flag, value)
}

func (gw *AppGateway) SetStudent(ctx context.Context, sm *models.Student) error {
	return gw.db.set(ctx, sm)
}
