package ports

import (
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/api/grpc/pb/answer"
)

type AnswerServer struct {
	answer.UnimplementedAnswerServiceServer
}

func (as *AnswerServer) SetAnswer(stream answer.AnswerService_SetAnswerServer) error {
	// for {
	// 	req, err := stream.Recv()
	// 	if err == io.EOF {
	// 		return stream.SendMsg(&answer.SetAnswerResponse{
	// 			Id:    req.GetId(),
	// 			Score: uint32(0),
	// 		})
	// 	}
	//
	// 	if err != nil {
	// 		log.Println(err.Error())
	// 		return err
	// 	}
	// 	answerModel := models.Answer{
	// 		Id:         req.GetId(),
	// 		Body:       req.GetBody(),
	// 		QuizId:     req.GetQuizId(),
	// 		StudentId:  req.GetStudentId(),
	// 		QuestionId: req.GetQuestionId(),
	// 	}
	// 	err = repository.SetAnswer(context.Background(), &answerModel)
	// 	if err != nil {
	// 		log.Println(err.Error())
	// 		return err
	// 	}
	// }
	return nil
}

func (as *AnswerServer) GetAnswersByStudent(stream answer.AnswerService_GetAnswersByStudentServer) error {
	// answers := make([]*answer.SetAnswerPerStudentResponse, 0)
	return nil
}

func (as *AnswerServer) GetAnswersByQuestion(stream answer.AnswerService_GetAnswersByQuestionServer) error {

	// answers := make([]*answer.SetAnswerPerQuestionResponse, 0)
	return nil
}

func (as *AnswerServer) SetScore(stream answer.AnswerService_SetScoreServer) error {
	return nil
}
