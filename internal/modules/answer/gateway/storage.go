package gateway

import (
	"context"
	"log"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/common/database/mariadb"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/utils/db"
)

type MainRepo interface {
	set(ctx context.Context, ans *models.Answer) error
	getByStudent(ctx context.Context, quizId string, enrollmentId int) ([]*models.QuizAnswersByStudent, error)
	getByQuiz(ctx context.Context, quizId string, enrollmentId int) ([]*models.QuizAnswersByStudent, error)
}

var mdbC, _ = mariadb.GetConnection()

type MariadbGW struct{}

func (mdbgw *MariadbGW) set(ctx context.Context, a *models.Answer) error {
	query := "INSERT INTO answers(id, body, enrollment_id, question_id) VALUES(?, ?, ?, ?)"
	_, err := mdbC.ExecContext(ctx, query, a.Id, a.Body, a.EnrollmentId, a.QuestionId)

	return err
}

func (mdbgw *MariadbGW) getByStudent(ctx context.Context, qz_id string, enr_id int) ([]*models.QuizAnswersByStudent, error) {
	answers, err := db.GetQuizAnswersByStudent(ctx, qz_id, enr_id, mdbC)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return answers, nil
}

func (mdbgw *MariadbGW) getByQuiz(ctx context.Context, qz_id string, enr_id int) ([]*models.QuizAnswersByStudent, error) {
	answers, err := db.GetQuizAnswersByQuiz(ctx, qz_id, enr_id, mdbC)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return answers, nil
}
