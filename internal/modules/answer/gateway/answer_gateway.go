package gateway

import (
	"context"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
)

type AppGateway struct {
	db MainRepo
}

var AnswerGW = AppGateway{db: &MariadbGW{}}

func (gw *AppGateway) SetAnswer(ctx context.Context, ans *models.Answer) error {
	return gw.db.set(ctx, ans)
}

func (gw *AppGateway) GetAnswersByStudent(ctx context.Context, quizId string, enrollmentId int) ([]*models.QuizAnswersByStudent, error) {
	return gw.db.getByStudent(ctx, quizId, enrollmentId)
}

func (gw *AppGateway) GetAnswersByQuiz(ctx context.Context, quizId string, enrollmentId int) ([]*models.QuizAnswersByStudent, error) {
	return gw.db.getByStudent(ctx, quizId, enrollmentId)
}
