// package server
//
// import (
// 	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/grpc/server/ports"
// )
//
// // Server serves gRPC requests.
// type Server struct {
// 	quizSV     ports.QuizServer
// 	studentSV  ports.StudentServer
// 	questionSV ports.QuestionServer
// 	answerSV   ports.AnswerServer
// }
//
// type Services struct {
// 	Store database.DBStore
// }
//
// func NewServer(svc *Services) (*Server, error) {
// 	sv := Server{
// 		quizSV:     ports.QuizServer{Store: svc.Store},
// 		studentSV:  ports.StudentServer{Store: svc.Store},
// 		questionSV: ports.QuestionServer{Store: svc.Store},
// 		answerSV:   ports.AnswerServer{Store: svc.Store},
// 	}
// 	return &sv, nil
// }

// func AddServices() *Services {
// 	return &Services{
// 		Store: database.NewDBStore("mariadb"),
// 	}
// }
//
// func NewAppRepository(dbr database.MainRepo) {
// 	gateway.NewStudentRepository(dbr)
// 	gateway.AddQuizRepository(dbr)
// }
