package database

import (
	"database/sql"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/common/database/mariadb"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/common/database/postgres"
)

type DBStore interface {
	OpenConnection() (*sql.DB, error)
}

func NewDBStore(store string) DBStore {
	switch store {
	case "mariadb":
		return mariadb.GetStore()
	case "postgres":
		return postgres.GetStore()
	}
	return nil
}
