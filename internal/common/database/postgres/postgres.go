package postgres

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/common/config/env"
)

type PostgresDBStore struct{}

var pgDBStore = PostgresDBStore{}

var dbconn, dberr = pgDBStore.OpenConnection()

func GetStore() *PostgresDBStore {
	return &pgDBStore
}

func FormatDSN() string {
	config := env.LoadDBConfig()
	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		config.Host, config.Port, config.Username, config.Password, config.DBName)
}

func (pg_repo *PostgresDBStore) OpenConnection() (*sql.DB, error) {
	dsn := FormatDSN()
	db, err := sql.Open("postgres", dsn)

	if err != nil {
		log.Fatal(err.Error())
		return db, nil
	}

	defer db.Close()
	CheckHealthDbConnection(db)

	return db, nil
}

func GetConnection() (*sql.DB, error) {
	err := dberr

	if err != nil {
		log.Fatal(err.Error())
	}

	return dbconn, nil
}

func CheckHealthDbConnection(dbC *sql.DB) int {
	err := dbC.Ping()

	if err != nil {
		return 0
	}
	fmt.Println("PGDB Connected!")
	return 1
}
