package mariadb

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/go-sql-driver/mysql"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/common/config/env"
)

type MariaDBStore struct{}

var mariaDBStore = MariaDBStore{}

var dbConn, dberr = mariaDBStore.OpenConnection()

func GetStore() *MariaDBStore {
	return &mariaDBStore
}

func FormatDSN() string {

	config := env.LoadDBConfig()

	cfg := mysql.Config{
		User:                 config.Username,
		Passwd:               config.Password,
		DBName:               config.DBName,
		Addr:                 config.Host + ":" + config.Port,
		Net:                  "tcp",
		AllowNativePasswords: true,
		ParseTime:            true,
	}
	return cfg.FormatDSN()
}

func (mDBStore *MariaDBStore) OpenConnection() (*sql.DB, error) {

	var db *sql.DB

	dsn := FormatDSN() // Get a database handle.
	var err error
	log.Println(dsn)
	db, err = sql.Open("mysql", dsn)

	if err != nil {
		log.Println(err.Error())
		log.Fatal(err)
	}

	pingErr := db.Ping()
	if pingErr != nil {
		log.Fatal(pingErr)
	}
	fmt.Println("mariaDB Connected!")

	return db, nil
}

func GetConnection() (*sql.DB, error) {
	err := dberr

	if err != nil {
		log.Fatal(err.Error())
	}

	return dbConn, nil
}
