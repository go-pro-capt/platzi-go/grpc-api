package env

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	DSNConfig      *DSNConfig
	GRPCServerAddr string `mapstructure:"GRPC_SERVER_ADDRESS"`
}

type DSNConfig struct {
	Username string
	Password string
	Port     string
	Host     string
	DBName   string
}

func LoadConfig(server string) *Config {
	dsn_conf := LoadDBConfig()
	grpc_addr := formatGrpcAddress(server, dsn_conf.Host)

	return &Config{
		DSNConfig:      dsn_conf,
		GRPCServerAddr: grpc_addr,
	}
}

func formatGrpcAddress(server string, host string) string {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err.Error())
	}
	var SVPort string
	switch server {
	case "student":
		SVPort = os.Getenv("SVPORT_STD")
		if SVPort == "" {
			SVPort = "9080"
		}
		return host + ":" + SVPort
	case "quiz":
		SVPort = os.Getenv("SVPORT_QZ")
		if SVPort == "" {
			SVPort = "8080"
		}
		return host + ":" + SVPort
	case "question":
		SVPort = os.Getenv("SVPORT_QS")
		if SVPort == "" {
			SVPort = "8080"
		}
		return host + ":" + SVPort
	case "answer":
		SVPort = os.Getenv("SVPORT_AS")
		if SVPort == "" {
			SVPort = "8080"
		}
		return host + ":" + SVPort
	case "enrollment":
		SVPort = os.Getenv("SVPORT_ENR")
		if SVPort == "" {
			SVPort = "8080"
		}
		return host + ":" + SVPort
	case "score":
		SVPort = os.Getenv("SVPORT_SCR")
		if SVPort == "" {
			SVPort = "8080"
		}
		return host + ":" + SVPort
	}
	return "No server selected"
}

func LoadDBConfig() *DSNConfig {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err.Error())
	}
	DBUser := os.Getenv("DBUSER")
	if DBUser == "" {
		DBUser = "root"
	}
	DBPass := os.Getenv("DBPASS")
	if DBPass == "" {
		DBPass = "psswd"
	}
	DBHost := os.Getenv("DBHOST")
	if DBHost == "" {
		DBHost = "localhost"
	}
	DBPort := os.Getenv("DBPORT")
	if DBPort == "" {
		DBPort = "4786"
	}
	DBName := os.Getenv("DBNAME")
	if DBName == "" {
		DBName = "testdb"
	}

	return &DSNConfig{
		Username: DBUser,
		Password: DBPass,
		Port:     DBPort,
		Host:     DBHost,
		DBName:   DBName,
	}
}
