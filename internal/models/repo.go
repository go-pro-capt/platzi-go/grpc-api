package models

type Model interface {
	Add()
}

func (*Student) Add()  {}
func (*Quiz) Add()     {}
func (*Question) Add() {}
func (*Answer) Add()   {}
