package models

import "time"

type Quiz struct {
	Id        string    `json:"id"`
	Name      string    `json:"name"`
	CreatedAt time.Time `json:"created_at"`
}

type QuizResults struct {
	QuizId    string `json:"quiz_id"`
	StudentId string `json:"student_id"`
	Score     int    `json:"score"`
}

type Question struct {
	Id        string `json:"id"`
	Name      string `json:"name"`
	Statement string `json:"statement"`
	QuizId    string `json:"test_id"`
}

type Answer struct {
	Id           string `json:"id"`
	Body         string `json:"body"`
	EnrollmentId int    `json:"enrollment_id"`
	QuestionId   string `json:"question_id"`
}

type QuizAnswersByStudent struct { // Not in DB
	Ans_id     string
	Ans_body   string
	Enr_quizid string
	Enr_id     int
}

type AnswerScore struct {
	Id       string `json:"id"`
	AnswerId string `json:"answer_id"`
	Score    int    `json:"score"`
}

type Enrollment struct {
	Id             int       `json:"id"`
	StudentId      string    `json:"student_id"`
	QuizId         string    `json:"quiz_id"`
	EnrollmentDate time.Time `json:"enrollment_date"`
	EnrollmentCode string    `json:"enrollment_code"`
}
