package models

type Student struct {
	Id   string `json:"id"`
	Name string `json:"name"`
	Age  string `json:"age"`
}
