package server

import (
	"log"
	"net"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/api/grpc/pb/answer"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/api/grpc/pb/enrollment"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/api/grpc/pb/question"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/api/grpc/pb/quiz"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/api/grpc/pb/score"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/api/grpc/pb/student"
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/common/config/env"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func RunGrpcServer(sv_flag string) {
	var config string
	// svs := AddServices()
	server, err := NewServer()

	if err != nil {
		log.Println(err.Error())
		log.Fatalf("Cannot Create a server %s", err.Error())
	}

	grpcServer := grpc.NewServer()
	switch sv_flag {
	case "student":
		config = env.LoadConfig("student").GRPCServerAddr
		student.RegisterStudentServiceServer(grpcServer, &server.studentSV)
	case "quiz":
		config = env.LoadConfig("quiz").GRPCServerAddr
		quiz.RegisterQuizServiceServer(grpcServer, &server.quizSV)
	case "question":
		config = env.LoadConfig("question").GRPCServerAddr
		question.RegisterQuestionServiceServer(grpcServer, &server.questionSV)
	case "answer":
		config = env.LoadConfig("answer").GRPCServerAddr
		answer.RegisterAnswerServiceServer(grpcServer, &server.answerSV)
	case "enrollment":
		config = env.LoadConfig("enrollment").GRPCServerAddr
		enrollment.RegisterEnrollmentServiceServer(grpcServer, &server.enrollmentSV)
	case "score":
		config = env.LoadConfig("score").GRPCServerAddr
		score.RegisterScoreServiceServer(grpcServer, &server.scoreSV)
	}

	reflection.Register(grpcServer)

	listener, err := net.Listen("tcp", config)
	if err != nil {
		log.Fatalf("Cannot Create a Listener %s", err.Error())
	}

	log.Printf("start gRPC server at %s", listener.Addr().String())
	err = grpcServer.Serve(listener)
	if err != nil {
		log.Fatalf("cannot start gRPC server %s", err.Error())
	}
}
