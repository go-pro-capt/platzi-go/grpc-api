package server

import (
	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/common/database"
	answer_ports "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/answer/ports"
	enrollment_gw "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/enrollment/gateway"
	enrollment_ports "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/enrollment/ports"
	question_gw "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/question/gateway"
	question_ports "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/question/ports"
	quiz_wg "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/quiz/gateway"
	quiz_ports "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/quiz/ports"
	score_gw "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/score/gateway"
	score_ports "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/score/ports"
	student_gw "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/student/gateway"
	student_ports "gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/modules/student/ports"
)

// Server serves gRPC requests.
type Server struct {
	quizSV       quiz_ports.QuizServer
	studentSV    student_ports.StudentServer
	enrollmentSV enrollment_ports.EnrollmentServer
	questionSV   question_ports.QuestionServer
	answerSV     answer_ports.AnswerServer
	scoreSV      score_ports.ScoreServer
}

type Services struct {
	Store database.DBStore
}

func NewServer() (*Server, error) {
	sv := Server{
		quizSV:       quiz_ports.QuizServer{Gw: quiz_wg.QuizGW},
		studentSV:    student_ports.StudentServer{Gw: student_gw.StudentGW},
		enrollmentSV: enrollment_ports.EnrollmentServer{Gw: enrollment_gw.EnrollmentGW},
		questionSV:   question_ports.QuestionServer{Gw: question_gw.QuestionGW},
		answerSV:     answer_ports.AnswerServer{},
		scoreSV:      score_ports.ScoreServer{Gw: score_gw.ScoreGW},
	}
	return &sv, nil
}

func AddServices() *Services {
	return &Services{
		Store: database.NewDBStore("mariadb"),
	}
}
