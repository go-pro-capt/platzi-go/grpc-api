package db

import (
	"context"
	"database/sql"
	"log"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
)

func FindStudentById(ctx context.Context, value string, conn *sql.DB) (models.Model, error) {
	query := "SELECT id, name, age FROM students WHERE id = ?"
	rows, err := conn.QueryContext(ctx, query, value)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = rows.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}()
	appModel, _, err := ScanModel(rows, "students", "id") // Student has been found By their ID
	if err != nil {
		var student = appModel.(*models.Student)
		log.Println(err.Error())
		return student, err
	}
	return appModel, nil
}

func GetAllStudents(ctx context.Context, conn *sql.DB) ([]*models.Student, error) {
	query := "SELECT id, name, age FROM students"
	rows, err := conn.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = rows.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}()
	students, err := ScanAllStudents(rows) // Student has been found By their ID
	if err != nil {
		log.Println(err.Error())
		return students, err
	}
	return students, nil
}

func FindQuizById(ctx context.Context, value string, conn *sql.DB) (models.Model, error) {
	query := "SELECT id, name FROM quizes WHERE id = ?"
	rows, err := conn.QueryContext(ctx, query, value)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = rows.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}()
	appModel, _, err := ScanModel(rows, "quizes", "id") // Quiz has been found By their ID
	if err != nil {
		var qz = appModel.(*models.Quiz)
		log.Println(err.Error())
		return qz, err
	}
	return appModel, nil
}

type Values struct {
	Id     string
	QuizId string
}

func FindQuestionById(ctx context.Context, values Values, conn *sql.DB) (*models.Question, error) {
	//values := Values{Id: value, QuizId: value}
	query := "SELECT id, name, statement FROM questions WHERE (id = ? and quiz_id = ?)"
	rows, err := conn.QueryContext(ctx, query, values.Id, values.QuizId)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = rows.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}()
	qsModel, err := ScanQuestionById(rows) // Quiz has been found By their ID
	if err != nil {
		log.Println(err.Error())
		return qsModel, err
	}
	return qsModel, nil
}

func GetQuestionsByQuizId(ctx context.Context, value string, conn *sql.DB) ([]*models.Question, error) {
	query := "SELECT id, name, statement FROM questions WHERE quiz_id = ?"
	rows, err := conn.QueryContext(ctx, query, value)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = rows.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}()
	questions, _ := ScanQuestionModel(rows)
	return questions, nil
}

func GetEnrollmentById(ctx context.Context, value string, conn *sql.DB) (*models.Enrollment, error) {
	//values := Values{Id: value, QuizId: value}
	query := "SELECT id, student_id, quiz_id, enrollment_date FROM enrollments WHERE quiz_id = ?"
	rows, err := conn.QueryContext(ctx, query, value)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = rows.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}()
	enrollmentModel, err := ScanEnrollmentById(rows) // Enrollments found By Enrollment ID
	if err != nil {
		log.Println(err.Error())
		return enrollmentModel, err
	}
	return enrollmentModel, nil
}

func GetEnrolledStudents(ctx context.Context, value string, conn *sql.DB) ([]*models.Enrollment, error) {
	//values := Values{Id: value, QuizId: value}
	query := "SELECT id, student_id, enrollment_date, enrollment_code FROM enrollments WHERE quiz_id = ?"
	rows, err := conn.QueryContext(ctx, query, value)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = rows.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}()
	enrollmentModel, err := ScanEnrollmentModel(rows) // Enrollments found By Quiz ID
	if err != nil {
		log.Println(err.Error())
		return enrollmentModel, err
	}
	return enrollmentModel, nil
}

func GetQuizAnswersByStudent(ctx context.Context, quizId string, enrollment_id int, conn *sql.DB) ([]*models.QuizAnswersByStudent, error) {
	query := `SELECT
									a.id,
    							e.enrollment_code,
    							e.student_id,
    							e.id
						FROM answers a
						INNER JOIN enrollments e
    				ON 
								e.id = a.enrollment_id
						WHERE 
								e.quiz_id = ?
						AND 
								e.id = ?`
	rows, err := conn.QueryContext(ctx, query, quizId, enrollment_id)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = rows.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}()
	answers, err := ScanQuizAnswersByStudent(rows, quizId, enrollment_id)
	if err != nil {
		log.Println(err.Error())
		return answers, err
	}
	return answers, nil
}

func GetQuizAnswersByQuiz(ctx context.Context, quizId string, enrollmentId int, conn *sql.DB) ([]*models.QuizAnswersByStudent, error) {
	query := `SELECT
    							a.id,
									a.body,
									e.student_id,
    							e.id
						FROM answers a
						INNER JOIN enrollments e
    				ON 
								e.id = a.enrollment_id
						WHERE 
								e.quiz_id = ?
						AND 
								e.id = ?`
	rows, err := conn.QueryContext(ctx, query, quizId, enrollmentId)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = rows.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}()
	answers, err := ScanQuizAnswersByStudent(rows, quizId, enrollmentId)
	if err != nil {
		log.Println(err.Error())
		return answers, err
	}
	return answers, nil
}
