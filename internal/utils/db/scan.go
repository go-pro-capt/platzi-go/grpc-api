package db

import (
	"database/sql"
	"log"

	"gitlab.com/go-pro-capt/platzi-go/grpc-api/internal/models"
)

func ScanModel(r *sql.Rows, db_table string, flag string) (models.Model, []models.Model, error) {
	var appModel models.Model
	var model_list []models.Model
	if db_table == "students" {
		std := models.Student{}
		if flag == "id" {
			for r.Next() {
				if err := r.Scan(&std.Id, &std.Name, &std.Age); err == nil {
					appModel = &std
					return appModel, nil, nil
				} else {
					log.Println(err.Error())
					return appModel.(*models.Student), nil, err
				}
			}

		}
	}
	if db_table == "quizes" {
		qz := models.Quiz{}
		if flag == "id" {
			for r.Next() {
				if err := r.Scan(&qz.Id, &qz.Name); err == nil {
					appModel = &qz
					return appModel, nil, nil
				} else {
					log.Println(err.Error())
					return appModel.(*models.Quiz), nil, err
				}
			}

		}
	}
	return appModel, model_list, nil
}

func ScanQuestionById(r *sql.Rows) (*models.Question, error) {
	qsModel := models.Question{}
	for r.Next() {
		if err := r.Scan(&qsModel.Id, &qsModel.Name, &qsModel.Statement); err == nil {
			return &qsModel, nil
		} else {
			log.Println(err.Error())
			return &qsModel, nil
		}
	}
	return &qsModel, nil
}

func ScanQuestionModel(r *sql.Rows) ([]*models.Question, error) {
	var question_list []*models.Question
	for r.Next() {
		qs := models.Question{}
		if err := r.Scan(&qs.Id, &qs.Name, &qs.Statement); err == nil {
			question_list = append(question_list, &qs)
		} else {
			log.Println(err.Error())
			return question_list, err
		}
	}
	return question_list, nil
}

func ScanAllStudents(r *sql.Rows) ([]*models.Student, error) {
	var student_list []*models.Student
	for r.Next() {
		std := models.Student{}
		if err := r.Scan(&std.Id, &std.Name, &std.Age); err == nil {
			student_list = append(student_list, &std)
		} else {
			log.Println(err.Error())
			return student_list, err
		}
	}
	return student_list, nil
}

func ScanEnrollmentById(r *sql.Rows) (*models.Enrollment, error) {
	enrollmentModel := models.Enrollment{}
	for r.Next() {
		if err := r.Scan(&enrollmentModel.Id, &enrollmentModel.StudentId, &enrollmentModel.QuizId, &enrollmentModel.EnrollmentDate); err == nil {
			return &enrollmentModel, nil
		} else {
			log.Println(err.Error())
			return &enrollmentModel, nil
		}
	}
	return &enrollmentModel, nil
}

func ScanEnrollmentModel(r *sql.Rows) ([]*models.Enrollment, error) {
	var enrollments []*models.Enrollment
	for r.Next() {
		enrollment := models.Enrollment{}
		if err := r.Scan(&enrollment.Id, &enrollment.StudentId, &enrollment.EnrollmentDate, &enrollment.EnrollmentCode); err == nil {

			enrollments = append(enrollments, &enrollment)
		} else {
			log.Println(err.Error())
			return enrollments, err
		}
	}
	return enrollments, nil
}

func ScanQuizAnswersByStudent(r *sql.Rows, quizId string, enrollment_id int) ([]*models.QuizAnswersByStudent, error) {

	var answers []*models.QuizAnswersByStudent
	for r.Next() {
		// answer := &models.Answer{}
		// enrollment := &models.Enrollment{}
		response := &models.QuizAnswersByStudent{}
		if err := r.Scan(&response.Ans_id, &response.Ans_body, &quizId, &enrollment_id); err == nil {
			// if err := r.Scan(&response.Ans_id, &response.Ans_body, &quizId, &studentId); err == nil {
			answers = append(answers, response)
		} else {
			log.Println(err.Error())
			return answers, err
		}

	} // end rows for
	return answers, nil
}
